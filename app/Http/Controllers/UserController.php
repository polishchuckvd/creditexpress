<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->only('login', 'password');
        $validator = Validator::make($data, User::$rules);

        if ($validator->fails()) {
            return ['error' => 'Bad credentials!'];
        } else {
            $token = Str::random(80);

            $user = new User([
                'login' => $data['login'],
                'password' => Hash::make('admin'),
                'api_token' => hash('sha256', $token),
            ]);

            $user->save();

            return ['token' => $token];
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('login', 'password');

        if (Auth::attempt($credentials, true)) {
            $user = User::where('login', $credentials['login'])->first();
            $token = Str::random(80);

            $user->forceFill([
                'api_token' => hash('sha256', $token),
            ])->save();

            return ['token' => $token];
        }

        return ['error' => 'Bad credentials!'];
    }
}
