<?php

namespace App\Http\Controllers;

use App\Models\CourseHistory;
use Illuminate\Http\Request;
use App\Models\Currency;
use Illuminate\Support\Facades\Validator;

class CurrencyController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $result = [];
        $model = Currency::with('course')->get();
        foreach ($model as $item) {
            $result[$item->name]['amount_sale'] = $item->course->amount_sale;
            $result[$item->name]['amount_purchase'] = $item->course->amount_purchase;
        }

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function update(Request $request)
    {
        $user = $request->user();
        $data = $request->only('currency', 'amount_sale', 'amount_purchase');

        $validator = Validator::make($data, [
            'currency' => 'required|exists:currency,name',
            'amount_sale' => 'required|numeric|between:0,99.99',
            'amount_purchase' => 'required|numeric|between:0,99.99',
        ]);

        if (!$validator->fails()) {
            $currency = Currency::where('name', $data['currency'])->first();

            if (isset($currency)) {
                (new CourseHistory([
                    'user_id' => $user->id,
                    'currency_id' => $currency->id,
                    'amount_sale' => $data['amount_sale'],
                    'amount_purchase' => $data['amount_purchase'],
                ]))->save();

                return redirect(route('home'));
            }
        }

        return ['error' => 'Bad request!'];
    }
}
