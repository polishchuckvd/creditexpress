<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseHistory extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'amount_sale',
        'amount_purchase',
        'user_id',
        'currency_id',
    ];

    /**
     * @var string
     */
    protected $table = 'course_history';
}
