<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Currency;
use App\Models\CourseHistory;

class ParseCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:parseCourse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = 'https://api.privatbank.ua/p24api/exchange_rates?json&date=' . date('d.m.Y');
        $data = json_decode(file_get_contents($url));

        foreach ($data->exchangeRate as $item) {
            if (isset($item->currency, $item->saleRate, $item->purchaseRate)) {
                $currencyModel = Currency::where('name',$item->currency)->first();

                if ($currencyModel === null) {
                    $currencyModel = new Currency([
                        'name' => $item->currency,
                        'user_id' => null
                    ]);
                    $currencyModel->save();
                }

                (new CourseHistory([
                    'amount_sale' => $item->saleRate,
                    'amount_purchase' => $item->purchaseRate,
                    'user_id' => null,
                    'currency_id' => $currencyModel->id
                ]))->save();
            }
        }

        return 0;
    }
}
