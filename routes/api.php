<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('login', 'UserController@authenticate')->name('login');
Route::get('create', 'UserController@create')->name('create');

Route::middleware(['auth:api'])->group(function () {
    Route::get('currency/update', 'CurrencyController@update');
});
